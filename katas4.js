const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";

const lotrCitiesArray = ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];

const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

function headerFuncao(n) {
    let header = document.createElement("div");
    header.textContent = "Kata " + n;
    document.body.appendChild(header);
    header.className = "titulo";
}


function writePage(x) {
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(x);
    document.body.appendChild(newElement);
    return x;
}

headerFuncao(1);

function kata1() {
    let newArray = gotCitiesCSV.split(",");
    writePage(newArray);
    return newArray;
}
kata1();

headerFuncao(2);

function kata2() {
    let newArray = bestThing.split(" ");
    writePage(newArray);
    return newArray;
}
kata2();

headerFuncao(3);

function kata3() {
    let newArray = gotCitiesCSV.split(",").join(";");
    writePage(newArray);
    return newArray;
}
kata3();

headerFuncao(4);

function kata4() {
    let newString = lotrCitiesArray.join(",");
    writePage(newString);
    return newString;
}
kata4();

headerFuncao(5);

function kata5() {
    let newArray = lotrCitiesArray.slice(0, 5);
    writePage(newArray);
    return newArray;
}
kata5();

headerFuncao(6);

function kata6() {
    let indiceInicial = lotrCitiesArray.length - 5;
    let newArray = lotrCitiesArray.slice(indiceInicial, lotrCitiesArray.length);
    writePage(newArray);
    return newArray;
}
kata6();

headerFuncao(7);

function kata7() {
    let newArray = lotrCitiesArray.slice(2, 5);
    writePage(newArray);
    return newArray;
}
kata7();

headerFuncao(8);

function kata8() {
    lotrCitiesArray.splice(2, 1);
    writePage(lotrCitiesArray);
    return lotrCitiesArray;
}
kata8();

headerFuncao(9);

function kata9() {
    lotrCitiesArray.splice(5, 2);
    writePage(lotrCitiesArray);
    return lotrCitiesArray;
}
kata9();

headerFuncao(10);

function kata10() {
    lotrCitiesArray.splice(2, 0, "Rohan");
    writePage(lotrCitiesArray);
    return lotrCitiesArray;
}
kata10();

headerFuncao(11);

function kata11() {
    lotrCitiesArray.splice(5, 1, 'Deadest Marshes');
    writePage(lotrCitiesArray);
    return lotrCitiesArray;
}
kata11();

headerFuncao(12);

function kata12() {
    let newArray = bestThing.slice(0, 14);
    writePage(newArray);
    return newArray;
}
kata12();

headerFuncao(13);

function kata13() {
    let newArray = bestThing.slice(bestThing.length - 12);
    writePage(newArray);
    return newArray;
}
kata13();

headerFuncao(14);

function kata14() {
    let firstIndex = bestThing.indexOf("boolean");
    let lastIndex = bestThing.lastIndexOf(" if");
    let resultado = bestThing.slice(firstIndex, lastIndex);
    writePage(resultado);
    return resultado;
}
kata14();


headerFuncao(15);

function kata15() {
    let result = bestThing.substring((bestThing.length - 12), bestThing.length);
    writePage(result);
    return result;
}
kata15();

headerFuncao(16);

function kata16() {
    let firstIndex = bestThing.indexOf("boolean");
    let lastIndex = bestThing.lastIndexOf(" if");
    let resultado = bestThing.substring(firstIndex, lastIndex);
    writePage(resultado);
    return resultado;
}
kata16();

headerFuncao(17);

function kata17() {
    lotrCitiesArray.pop();
    writePage(lotrCitiesArray);
    return lotrCitiesArray;
}
kata17();

headerFuncao(18);

function kata18() {
    lotrCitiesArray.push("Deadest Marshes");
    writePage(lotrCitiesArray);
    return lotrCitiesArray;
}
kata18();

headerFuncao(19);

function kata19() {
    lotrCitiesArray.shift();
    writePage(lotrCitiesArray);
    return lotrCitiesArray;
}
kata19();

headerFuncao(20);

function kata20() {
    lotrCitiesArray.unshift("Mordor");
    writePage(lotrCitiesArray);
    return lotrCitiesArray;
}
kata20();


headerFuncao("Extra 1");

function extra1() {
    writePage(bestThing.indexOf("only"));
    return bestThing.indexOf("only");
}
extra1();

headerFuncao("Extra 2");

function extra2() {
    writePage(bestThing.lastIndexOf("", bestThing.length - 1));
    return bestThing.lastIndexOf("", bestThing.length - 1);

}
extra2();

headerFuncao("Extra 3");

function extra3() {
    let myReg = new RegExp("([a,e,i,o,u])\\1");
    let newArray = gotCitiesCSV.split(",");
    let newArrayEscrito = [];
    for (let count = 0; count < newArray.length; count++) {
        if (newArray[count].search(myReg) != -1) {
            newArrayEscrito.push(newArray[count]);
        }
    }
    writePage(newArrayEscrito);
    return newArrayEscrito;
}
extra3();

headerFuncao("Extra 4");

function extra4() {
    let myReg = new RegExp(/or$/);
    let newArrayEscrito = [];
    for (let count = 0; count < lotrCitiesArray.length; count++) {
        if (lotrCitiesArray[count].search(myReg) > 0) {
            newArrayEscrito.push(lotrCitiesArray[count]);
        }
    }
    writePage(newArrayEscrito);
    return newArrayEscrito;
}
extra4();


headerFuncao("Extra 5");

function extra5() {
    let myReg = new RegExp(/^b/);
    let newArray = bestThing.split(" ");
    let newArrayEscrito = [];
    for (let count = 0; count < newArray.length; count++) {
        if (newArray[count].search(myReg) == 0) {
            newArrayEscrito.push(newArray[count]);
        }
    }
    writePage(newArrayEscrito);
    return newArrayEscrito;

}
extra5();

headerFuncao("Extra 6");

function extra6() {
    writePage(lotrCitiesArray.includes("Mirkwood"));
    return lotrCitiesArray.includes("Mirkwood")
}
extra6();

headerFuncao("Extra 7");

function extra7() {
    writePage(lotrCitiesArray.includes("Hollywood"));
    return lotrCitiesArray.includes("Hollywood");
}
extra7();

headerFuncao("Extra 8");

function extra8() {
    writePage(lotrCitiesArray.indexOf("Mirkwood"));
    return lotrCitiesArray.indexOf("Mirkwood");
}
extra8();

headerFuncao("Extra 9");

function extra9() {
    let myReg = new RegExp(/ /);
    for (let count = 0; count < lotrCitiesArray.length; count++) {
        if (lotrCitiesArray[count].search(myReg) > 0) {
            writePage(lotrCitiesArray[count]);
            return lotrCitiesArray[count];
        }
    }
}
extra9();

headerFuncao("Extra 10");
function extra10() {
    writePage(lotrCitiesArray.reverse());
    return lotrCitiesArray.reverse();
}
extra10();

headerFuncao("Extra 11");
function extra11() {
    writePage(lotrCitiesArray.sort());
    return lotrCitiesArray.sort();
}
extra11();


function comparar(a, b) {
    if (a.length < b.length) {
        return -1
    }
    if (a.length > b.length) {
        return 1
    }
    return 0; 
}

headerFuncao("Extra 12");
function extra12() {
    writePage(lotrCitiesArray.sort(comparar));
    return lotrCitiesArray.sort(comparar);
}
 extra12();